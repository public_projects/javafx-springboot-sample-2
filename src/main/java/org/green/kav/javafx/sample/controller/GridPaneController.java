package org.green.kav.javafx.sample.controller;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.springframework.stereotype.Service;

@Service
public class GridPaneController {

    private GridPane gridPane;

    public  GridPaneController(){
        gridPane = new GridPane();
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public void build() {
        Button button = new Button("b1");
        Label label = new Label("Lbl");
        gridPane.add(button, 0, 1);
        gridPane.add(label, 1, 1);
    }
}
