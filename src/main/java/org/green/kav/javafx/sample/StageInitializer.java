package org.green.kav.javafx.sample;

import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.green.kav.javafx.sample.controller.GridPaneController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StageInitializer implements ApplicationListener<JavafxSpringBootUIApplication.StageReadyEvent> {
    private String applicationTitle;
    private ApplicationContext applicationContext;

    @Autowired
    private GridPaneController gridPaneController;

    public StageInitializer(@Value("${spring.application.ui.title}") String applicationTitle,
                            ApplicationContext applicationContext) {
        this.applicationTitle = applicationTitle;
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(JavafxSpringBootUIApplication.StageReadyEvent stageReadyEvent) {
            Stage stage = stageReadyEvent.getStage();
            GridPane gridPane = gridPaneController.getGridPane();
            stage.setScene(new Scene(gridPane, 800, 600));
            stage.setTitle(applicationTitle);
            stage.show();
            gridPaneController.build();
    }
}
